# Install tools
sudo apt-get install pavucontrol 
sudo apt-get install alsa-tools-gui

# Setup correct pins to support all speakers
hdajackretask

## Settings
"Select a codec": Realtek ALC 295
check "Show unconnected pins"
check "Advanced override"

Apply the following settings:
0x14: 
Jack | Front | Speaker | Other Analog
Unknown | Not Present | 5 | Front

0x17:
No override

Jack | Internal | Speaker | Other Analog
Unknown | Not Present | 1 | Front

0x1e:
Jack | Rear | Speaker | Other Analog
Green |  Not Present | 5 | Front

apply "Install boot override"
reboot 


# Known issues and fixes 
## On startup no speakers detected
sudo alsa force-reload
