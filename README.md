# Setup Elementary OS 5 on HP Envy 13 ad1xx /w Dell DW1560

## Specs
CPU: Intel Core i7 8550u
RAM: 8 GB DDR4
Graphics: Intel UHD620 + NVidia MX150 
Sound: Realtek ALC295 - 4 speakers Bang & Olufsen
Wifi/Bluetooth: Dell DW1560 (replaced for MacOS installation)
Display: 13.3" FHD IPS Panel

## Drivers & Fixes
[Wireless](wireless/README.md)
[Bluetooth](bluetooth/README.md)
[Speakers](sound/README.md)
[Graphics](graphics.README.md)

## Known issues
SD Card reader





